require 'rails_helper'

RSpec.describe ParkingLot, type: :model do

	before(:each) do
        
            pls = [
                {:street_address => "Vonemuise 15", :price_per_hour => 0.80, :free_lots => 15},
                {:street_address => "Turu 2", :price_per_hour => 1.00, :free_lots => 30},
            ]

            pls.each do |pl|
              ParkingLot.create!(pl)
            end
        
    end

	context "ParkingLot" do
		it "get all ParkingLot s" do
			
		  	expect(ParkingLot.all().size).to be(2)
			
		end

        it "adds a ParkingLot" do
            
            pl = ParkingLot.new()
            pl.street_address = 'Raatuse 21'
            pl.save
            expect(ParkingLot.all().size).to be(3)
            
        end

        it "deletes a ParkingLot" do
            
            pl = ParkingLot.find(1)
            pl.destroy
            expect(ParkingLot.all().size).to be(1)
            
        end
	end

		  	
end
