Feature: User can find parking lot near a location he providing


Background: locations in database
 
	Given the following movies exist:
	| street_address | price_per_hour | free_lots |
	| Vonemuise 15   | 0.80     	  | 15 		  |
	| Turu 2 		 | 1.00     	  | 30 	 	  |
	| Raatuse 21     | 0.50     	  | 20 		  |


Scenario: Finding a paking lot
    When I am looking for to a parking lot close to Raatuse 21 
    And I open the FindMeALot website
    And entering 'Raatuse 21' as reference address
    And tapping submit button
    Then I should see a number of parking lots