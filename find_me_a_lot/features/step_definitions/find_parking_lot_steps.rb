Given(/^the following movies exist:$/) do |table|
  
  	table.hashes.each do |lot|

  		ParkingLot.create(lot)

  	end
  
end

When(/^I am looking for to a parking lot close to Raatuse (\d+)$/) do |arg1|
  	
end

When(/^I open the FindMeALot website$/) do
  	visit('parking_lots/form')
end

When(/^entering 'Raatuse (\d+)' as reference address$/) do |arg1|
	fill_in('ref_addr', with: 'Raatuse 21')
end

When(/^tapping submit button$/) do
  	click_on('Submit')
end

Then(/^I should see a number of parking lots$/) do
  	expect(page).to have_content('Vonemuise 15')
  	expect(page).to have_content('Turu 2')
  	expect(page).to have_content('Raatuse 21')
  	#page.body.should =~ /Vonemuise 15.*Turu 2.*Raatuse 21/
end
