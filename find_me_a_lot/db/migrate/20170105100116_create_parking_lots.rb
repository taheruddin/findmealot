class CreateParkingLots < ActiveRecord::Migration
  def change
    create_table :parking_lots do |t|

    	t.string 'street_address'
    	t.float 'price_per_hour'
    	t.integer 'free_lots'

    	t.timestamps
    end
  end
end
