# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)




pls = [
    {:street_address => "Vonemuise 15", :price_per_hour => 0.80, :free_lots => 15},
    {:street_address => "Turu 2", :price_per_hour => 1.00, :free_lots => 30},
    {:street_address => "Raatuse 21", :price_per_hour => 0.50, :free_lots => 20},
]

pls.each do |pl|
  ParkingLot.create!(pl)
end